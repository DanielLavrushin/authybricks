﻿using Newtonsoft.Json;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using Newtonsoft;

namespace AuthyBricks.Shared.Steam
{
    public class GameInfo
    {
        [JsonProperty("appid")]
        public long Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; } = string.Empty;
        [JsonProperty("short_description")]
        public string Description { get; set; } = string.Empty;

        public bool IsAvailable { get; set; } = true;

        public bool HasPicture { get; set; } = true;
        public string Image => HasPicture ? $"https://cdn.cloudflare.steamstatic.com/steam/apps/{Id}/header.jpg" : "https://cdn.cloudflare.steamstatic.com/steam/apps/767/header.jpg";
    }
}
