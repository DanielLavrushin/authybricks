﻿using Microsoft.AspNetCore.Identity;

using System.ComponentModel.DataAnnotations.Schema;

namespace AuthyBricks.Shared
{
    [Serializable]
    public class AuthyBricksUser : IdentityUser
    {
        public string? FullName { get; set; }

        public bool? IsSubscribed { get; set; }
        public string? Theme { get; set; }
        public string? GamesData { get; set; }

        private AuthyBricksGames _games;

        [NotMapped]
        public string[] Roles { get; set; }

        public bool IsInRole(string role)
        {
            return Roles.Contains(role);
        }

        [NotMapped]
        public AuthyBricksGames? Games
        {
            get
            {
                if (_games == null)
                {
                    if (string.IsNullOrWhiteSpace(GamesData))
                    {
                        _games = new AuthyBricksGames();
                    }
                    else
                    {
                        _games = Newtonsoft.Json.JsonConvert.DeserializeObject<AuthyBricksGames>(GamesData);
                    }

                }
                return _games;
            }
            set
            {
                GamesData = Newtonsoft.Json.JsonConvert.SerializeObject(value);
                _games = value;
            }
        }
    }

    [Serializable]
    public class AuthyBricksGames
    {
        public long[] GameIds { get; set; }
    }
}
