﻿using AuthyBricks.Shared;

using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace AuthyBricks.Identity.Data
{
    public class ApplicationDbContext : IdentityDbContext<AuthyBricksUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
    }
}