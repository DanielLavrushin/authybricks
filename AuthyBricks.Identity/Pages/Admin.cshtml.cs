﻿using AuthyBricks.Shared;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using AuthyBricks.Identity.Api.Configuration;

namespace AuthyBricks.Identity.Pages
{
    [Authorize]
    public class AdminModel : PageModel
    {

        private readonly ILogger<IndexModel> _logger;
        UserManager<AuthyBricksUser> userManager;
        RoleManager<IdentityRole> roleManager;

        public ICollection<AuthyBricksUser> Users { get; set; }
        public ICollection<IdentityRole> Roles { get; set; }

        public AdminModel(ILogger<IndexModel> logger, UserManager<AuthyBricksUser> userManager, RoleManager<IdentityRole> roleManager)
        {
            this.userManager = userManager;
            this.roleManager = roleManager;
        }

        public async Task OnGetAsync()
        {
            if (await userManager.IsInRoleAsync(User, "admin") != true)
                Redirect("/");

            Users = userManager.Users.ToList();
            Roles = roleManager.Roles.ToList();
            foreach (var user in Users)
            {
                var roles = await userManager.GetRolesAsync(user);
                user.Roles = roles.ToArray();
            }
        }

        public async Task<IActionResult> OnPostChangeRoleAsync([FromBody] UserRoleModel model)
        {
            if (await userManager.IsInRoleAsync(User, "admin") != true)
                Redirect("/");

            var user = await userManager.FindByIdAsync(model.UserId);

            if (user == null)
            {
                HttpContext.Response.StatusCode = 404;
                return new EmptyResult();
            }

            var roles = await userManager.GetRolesAsync(user);
            await userManager.RemoveFromRolesAsync(user, roles);
            await userManager.AddToRoleAsync(user, model.RoleId);

            return new EmptyResult();
        }

    }

    public class UserRoleModel
    {
        public string UserId { get; set; }
        public string RoleId { get; set; }
    }
}
