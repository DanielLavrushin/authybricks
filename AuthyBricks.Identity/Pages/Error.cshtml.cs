using IdentityServer4.Services;

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.VisualBasic;

using System.Diagnostics;

namespace AuthyBricks.Identity.Pages
{
    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    [IgnoreAntiforgeryToken]
    public class ErrorModel : PageModel
    {
        public string? Message { get; set; }
        public string? RequestId { get; set; }
        private readonly IIdentityServerInteractionService _interaction;

        public bool ShowRequestId => !string.IsNullOrEmpty(RequestId);

        private readonly ILogger<ErrorModel> _logger;

        public ErrorModel(ILogger<ErrorModel> logger, IIdentityServerInteractionService interaction)
        {
            _interaction = interaction;
            _logger = logger;
        }

        public async Task OnGet(string errorId)
        {
            var message = await _interaction.GetErrorContextAsync(errorId);
            if (message != null)
            {
                Message = message.ErrorDescription;
            }
            RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier;
        }
    }
}