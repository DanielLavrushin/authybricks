﻿using AuthyBricks.Shared;

using Microsoft.AspNetCore.Identity;

namespace AuthyBricks.Identity.Api.Configuration
{
    public class RolesMiddleware
    {
        IServiceProvider provider;
        private readonly RequestDelegate _next;
        public RolesMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext context, IServiceProvider provider)
        {
            this.provider = provider;
            var roleManager = GetServiceFromCollection<RoleManager<IdentityRole>>();
            string[] roleNames = { "admin", "manager", "member" };

            foreach (var roleName in roleNames)
            {
                var roleExist = await roleManager.RoleExistsAsync(roleName);
                if (!roleExist)
                {
                    await roleManager.CreateAsync(new IdentityRole(roleName));
                }
            }

            await CreateUser("admin");
            await CreateUser("manager");
            await CreateUser("member");
            await _next(context);

        }
        private T GetServiceFromCollection<T>()
        {
            return provider.GetService<T>();
        }
        public async Task CreateUser(string name)
        {
            var userManager = GetServiceFromCollection<UserManager<AuthyBricksUser>>();
            var mail = $"{name}@authybricks.lcl";
            var user = await userManager.FindByEmailAsync(mail);
            if (user == null)
            {
                var createUser = await userManager.CreateAsync(new AuthyBricksUser() { Email = mail, EmailConfirmed = true, UserName = mail }, "Qwerty123$");
                if (createUser.Succeeded)
                {
                    user = await userManager.FindByEmailAsync(mail);
                    await userManager.AddToRoleAsync(user, name);
                }
            }
        }
    }
}
