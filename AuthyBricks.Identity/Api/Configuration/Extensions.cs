﻿using AuthyBricks.Shared;

using Microsoft.AspNetCore.Identity;

using System.Security.Claims;
using System.Security.Principal;

namespace AuthyBricks.Identity.Api.Configuration
{
    public static class Extensions
    {
        public static async Task<bool> IsInRoleAsync(this UserManager<AuthyBricksUser> userManager, ClaimsPrincipal user, string role)
        {
            var userId = user.FindFirstValue("sub");
            var usr = await userManager.FindByIdAsync(userId);
            if (usr == null) return false;

            return await userManager.IsInRoleAsync(usr, role);

        }
        public static IApplicationBuilder CreateAuthyBrickRoles(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<RolesMiddleware>();
        }
    }
}
