﻿using IdentityServer4;
using IdentityServer4.Models;

namespace AuthyBricks.Identity.Api.Configuration
{
    public class Resources
    {
        public static IEnumerable<IdentityResource> GetIdentityResources()
        {
            return new[]
            {
            new IdentityResources.OpenId(),
            new IdentityResources.Profile(),
            new IdentityResources.Email(),
            new IdentityResource
            {
                Name = "role",
                UserClaims = new List<string> {"role"}
            }
        };
        }
        public static IEnumerable<ApiResource> GetApiResources()
        {
            return new[]
            {
            new ApiResource
            {
                Name = "authyBricksGamesApi",
                DisplayName = "authyBricks Games Api",
                Description = "Allow the application to access authyBricks Games Api on your behalf",
                Scopes = new List<string> {  "authyBricks.Games.users"},
                ApiSecrets = new List<Secret> {new Secret("password123".Sha256())},
                UserClaims = new List<string> {"role"}
            }
        };
        }
    }
}
