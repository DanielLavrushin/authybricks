﻿using IdentityServer4;
using IdentityServer4.Models;

namespace AuthyBricks.Identity.Api.Configuration
{
    public class Clients
    {
        public static string secret = "";
        public static IEnumerable<Client> Get()
        {
            return new List<Client>()
            {
                     new Client
                {
                    ClientId = "authyBricks.Games.Api",
                    ClientName = "authyBricks games users Api",
                    AllowedGrantTypes = GrantTypes.ClientCredentials,
                    ClientSecrets = new List<Secret> {new Secret(secret.Sha256())},
                    AllowedScopes = new List<string> { "authyBricks.Games.users" }
                     },
                new Client
                {
                    ClientId = "authyBricks.Games",
                    ClientName = "authyBricks.Games Web App",
                    ClientSecrets = new List<Secret> { new Secret(secret.Sha256()) },

                    AllowedGrantTypes = GrantTypes.Code,
                    RedirectUris = new List<string> { "https://localhost:7001/signin-oidc" },
                    AllowedScopes = new List<string>
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        IdentityServerConstants.ExternalCookieAuthenticationScheme,
                        IdentityServerConstants.StandardScopes.Email,
                        "role",
                        "authyBricks.Games.users",
                    },
                    RequirePkce = true,
                    AllowPlainTextPkce = false
                }
            };
        }
    }
}
