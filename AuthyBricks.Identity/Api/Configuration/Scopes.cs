﻿using IdentityServer4.Models;

using System.Data;

namespace AuthyBricks.Identity.Api.Configuration
{
    public class Scopes
    {
        public static IEnumerable<ApiScope> GetApiScopes()
        {
            return new[]
{
            new ApiScope("authyBricks.Games.users", "Read  authyBricks users API"),

        };
        }
    }
}
