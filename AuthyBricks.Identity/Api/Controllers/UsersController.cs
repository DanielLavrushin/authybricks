﻿using AuthyBricks.Shared;

using Microsoft.AspNetCore.Authentication.OAuth;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.Extensions.Logging;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AuthyBricks.Identity.Api.Controllers
{

    [ApiController]
    [Route("api/[controller]")]
    [Authorize(AuthenticationSchemes = "Bearer", Policy = "authyBricks.Games.Api")]
    public class UsersController : ControllerBase
    {
        private readonly UserManager<AuthyBricksUser> _userManager;
        private readonly IUserStore<AuthyBricksUser> _userStore;
        public UsersController(UserManager<AuthyBricksUser> userManager, IUserStore<AuthyBricksUser> userStore)
        {
            _userManager = userManager;
            _userStore = userStore;
        }

        [HttpGet]
        public async Task<IEnumerable<AuthyBricksUser>> Get()
        {
            var users = await Task.Run(() => _userManager.Users.ToArray());
            return users;
        }

        [HttpPut]
        public async Task<AuthyBricksUser> Put([FromBody] AuthyBricksUser user)
        {
            var dbUser = await GetUser(user.Id);
            if (dbUser != null)
            {
                dbUser.FullName = user.FullName;
                dbUser.IsSubscribed = user.IsSubscribed;
                dbUser.Theme = user.Theme;
                dbUser.GamesData = Newtonsoft.Json.JsonConvert.SerializeObject(user.Games);
                await _userManager.UpdateAsync(dbUser);
            }
            return dbUser;
        }

        [HttpGet]
        [Route("{userId}")]
        public async Task<AuthyBricksUser> GetUser(string userId)
        {
            var usr = await _userManager.FindByIdAsync(userId);
            var roles = await _userManager.GetRolesAsync(usr);
            usr.Roles = roles.ToArray();
            return usr;
        }

        public override BadRequestObjectResult BadRequest([ActionResultObjectValue] object? error)
        {
            return base.BadRequest(error);
        }
    }
}
