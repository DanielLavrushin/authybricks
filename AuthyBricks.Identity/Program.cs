using AuthyBricks.Identity.Data;
using AuthyBricks.Identity.Api.Configuration;
using IdentityServer4.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

using static System.Formats.Asn1.AsnWriter;
using Microsoft.AspNetCore.Authentication.Cookies;
using AuthyBricks.Shared;
using Microsoft.Extensions.DependencyInjection;
using IdentityModel;
using Microsoft.AspNetCore.Authentication;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;

var builder = WebApplication.CreateBuilder(args);

Clients.secret = builder.Configuration.GetValue<string>("Secret");


var connectionString = builder.Configuration.GetConnectionString("DefaultConnection");
builder.Services.AddDbContext<ApplicationDbContext>(options => options.UseSqlServer(connectionString));

builder.Services.AddDatabaseDeveloperPageExceptionFilter();

builder.Services.AddDefaultIdentity<AuthyBricksUser>(options =>
{
    options.SignIn.RequireConfirmedAccount = true;
    options.Password.RequireDigit = false;
    options.Password.RequireNonAlphanumeric = false;
    options.Password.RequireLowercase = false;
    options.Password.RequireNonAlphanumeric = false;
    options.Password.RequiredLength = 1;
    options.Password.RequiredUniqueChars = 0;
    options.ClaimsIdentity.RoleClaimType = "role";
})
    .AddRoles<IdentityRole>()
    .AddEntityFrameworkStores<ApplicationDbContext>()
    .AddTokenProvider<DataProtectorTokenProvider<AuthyBricksUser>>(TokenOptions.DefaultProvider);


builder.Services.AddIdentityServer(options =>
{
    options.UserInteraction.ErrorUrl = "/error";
    options.UserInteraction.LoginUrl = "/identity/account/login";
})
       .AddInMemoryClients(Clients.Get())
       .AddInMemoryIdentityResources(AuthyBricks.Identity.Api.Configuration.Resources.GetIdentityResources())
       .AddInMemoryApiResources(AuthyBricks.Identity.Api.Configuration.Resources.GetApiResources())
       .AddInMemoryApiScopes(Scopes.GetApiScopes())
       .AddDeveloperSigningCredential();

builder.Services.AddAuthentication()
    .AddJwtBearer(options =>
  {
      options.SaveToken = true;
      options.Authority = "https://localhost:7000";
      options.TokenValidationParameters.ValidateAudience = false;
  });
builder.Services.AddAuthorization(options =>
{
    options.AddPolicy("authyBricks.Games.Api", policy =>
    {
        policy.RequireAuthenticatedUser();
        policy.RequireClaim("scope", "authyBricks.Games.users");
    });
});
builder.Services.Configure<ApiBehaviorOptions>(options =>
{
    options.SuppressModelStateInvalidFilter = true;
});
builder.Services.AddControllersWithViews().AddRazorRuntimeCompilation();
builder.Services.AddRazorPages();

var app = builder.Build();
//app.CreateAuthyBrickRoles();
if (app.Environment.IsDevelopment())
{
    app.UseMigrationsEndPoint();
}
else
{
    app.UseExceptionHandler("/Error");
    app.UseHsts();
}
app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();
app.UseCookiePolicy();
app.UseIdentityServer();

JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();
JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Add("sub", ClaimTypes.NameIdentifier);
app.UseAuthentication();
app.UseAuthorization();

app.MapRazorPages();

app.UseEndpoints(endpoints =>
{
    endpoints.MapControllers().RequireAuthorization("authyBricks.Games.Api");
});
app.Run();
