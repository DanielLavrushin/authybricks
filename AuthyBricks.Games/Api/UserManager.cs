﻿using AuthyBricks.Shared;

using IdentityModel.Client;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

using System.Security.Claims;
using System.Text;
using AuthyBricks.Shared.Steam;

namespace AuthyBricks.Games.Api
{
    public interface IUserManager
    {
        AuthyBricksUser CurrentUser { get; }
        Task<bool> IsSignedIn(ClaimsPrincipal user);
        Task<AuthyBricksUser> GetUserAsync(string? userId = null);
        Task<AuthyBricksUser> UpdateAsync(AuthyBricksUser user);
        Task<AuthyBricksUser?> ToggleGameForUser(AuthyBricksUser currentUser, GameInfo? game);
    }
    public class UserManager : IUserManager
    {
        IHttpContextAccessor httpContextAccessor;
        IIdentityService identityService;
        public AuthyBricksUser CurrentUser { get; private set; }

        public UserManager(IHttpContextAccessor httpContextAccessor, IIdentityService identityService)
        {
            this.httpContextAccessor = httpContextAccessor;
            this.identityService = identityService;


        }
        public async Task<bool> IsSignedIn(ClaimsPrincipal user)
        {
            var isSignedIn = user?.Identity?.IsAuthenticated ?? false;

            if (!isSignedIn) return false;

            var userId = TryGetValue(ClaimTypes.NameIdentifier);

            CurrentUser = await GetUserAsync();
            return CurrentUser != null && CurrentUser.Id == userId;
        }
        public async Task<AuthyBricksUser?> ToggleGameForUser(AuthyBricksUser user, GameInfo? game)
        {
            var locaUser = await GetUserAsync(user.Id);
            if (locaUser != null && game != null)
            {
                var ids = locaUser.Games?.GameIds?.ToList() ?? new List<long>();
                if (ids.Contains(game.Id))
                {
                    ids.Remove(game.Id);
                }
                else
                {
                    ids.Add(game.Id);
                }
                locaUser.Games.GameIds = ids.ToArray();
                locaUser = await UpdateAsync(locaUser);
                return locaUser;
            }

            return null;
        }

        public async Task<AuthyBricksUser> GetUserAsync(string? userId = null)
        {
            userId = userId ?? TryGetValue(ClaimTypes.NameIdentifier);
            var token = await identityService.GetTokenAsync();
            using (var client = new HttpClient())
            {
                client.SetBearerToken(token.AccessToken);
                var result = await client.GetAsync($"https://localhost:7000/api/users/{userId}");
                if (result.IsSuccessStatusCode)
                {
                    var model = await result.Content.ReadAsStringAsync();
                    return CurrentUser = JsonConvert.DeserializeObject<AuthyBricksUser>(model);
                }
                return null;
            }
        }

        public async Task<AuthyBricksUser> UpdateAsync(AuthyBricksUser user)
        {

            var token = await identityService.GetTokenAsync();
            using (var client = new HttpClient())
            {
                client.SetBearerToken(token.AccessToken);

                var result = await client.PutAsync($"https://localhost:7000/api/users", new StringContent(JsonConvert.SerializeObject(user), Encoding.UTF8, "application/json"));
                if (result.IsSuccessStatusCode)
                {
                    var model = await result.Content.ReadAsStringAsync();
                    return CurrentUser = JsonConvert.DeserializeObject<AuthyBricksUser>(model);
                }
                return null;
            }
        }

        internal string? TryGetValue(string claim)
        {
            return httpContextAccessor.HttpContext?.User?.FindFirst(claim)?.Value ?? null;
        }

    }
}
