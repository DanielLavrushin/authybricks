﻿using IdentityModel.Client;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace AuthyBricks.Games.Api
{
    public interface IIdentityService
    {
        Task<TokenResponse> GetTokenAsync();
    }

    public class IdentityService : IIdentityService
    {
        private DiscoveryDocumentResponse _discoveryDocument { get; set; }
        public IdentityService()
        {
            using (var client = new HttpClient())
            {
                _discoveryDocument = client.GetDiscoveryDocumentAsync("https://localhost:7000/.well-known/openid-configuration").Result;
            }
        }

        public async Task<TokenResponse> GetTokenAsync()
        {
            using (var client = new HttpClient())
            {
                var tokenResponse = await client.RequestClientCredentialsTokenAsync(new ClientCredentialsTokenRequest
                {
                    Address = _discoveryDocument.TokenEndpoint,
                    ClientId = "authyBricks.Games.Api",
                    Scope = "authyBricks.Games.users",
                    ClientSecret = "password123"
                });
                if (tokenResponse.IsError)
                {
                    throw new Exception("Token Error");
                }
                return tokenResponse;
            }
        }
    }
}
