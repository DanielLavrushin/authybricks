﻿namespace AuthyBricks.Games.Api
{
    public class CheckSteamDbImageService : BackgroundService
    {
        private readonly SteamDbTaskWorker queue;

        public CheckSteamDbImageService(SteamDbTaskWorker queue)
        {
            this.queue = queue;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                var workItem = await queue.DequeueAsync(stoppingToken);

                await workItem(stoppingToken);
            }
        }
    }
}
