﻿using AuthyBricks.Shared;
using AuthyBricks.Shared.Steam;
using IdentityModel.Client;
using Newtonsoft.Json.Linq;

using Newtonsoft.Json;
using System.Net;
using Microsoft.Extensions.Logging;

namespace AuthyBricks.Games.Api
{

    public interface ISteamDbService : IHostedService
    {
        IDictionary<long, GameInfo> Apps { get; set; }
        Task<IDictionary<long, GameInfo>?> GetAppsAsync();
        Task<IEnumerable<GameInfo>?> SearchAsync(string pattern);
        IList<GameInfo>? GetAppsByIds(params long[] id);
        GameInfo? GetAppById(long id);
        Task<GameInfo?> GetAppInfoAsync(long appId);
    }
    public class SteamDbService : BackgroundService, ISteamDbService
    {
        const string ApiKey = "5CA81690DF208B82DA66A2882F9FC9BB";
        private readonly SteamDbTaskWorker worker;

        public IDictionary<long, GameInfo> Apps { get; set; }
        ILogger logger;
        public SteamDbService(ILogger<SteamDbService> logger, SteamDbTaskWorker worker)
        {
            this.logger = logger;
            this.worker = worker;
        }

        public async Task<IDictionary<long, GameInfo>?> GetAppsAsync()
        {
            if (Apps != null) return Apps;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls13 | SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
            ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;
            using (var client = new HttpClient())
            {
                var result = await client.GetAsync($"https://api.steampowered.com/ISteamApps/GetAppList/v2/?key={ApiKey}");
                if (result.IsSuccessStatusCode)
                {
                    var model = await result.Content.ReadAsStringAsync();
                    var obj = JObject.Parse(model);


                    var apps = obj["applist"]["apps"].ToObject<IEnumerable<GameInfo>>();
                    var name = new string("ogel".Reverse().ToArray());
#if !ALLGAMES
                    apps = apps.Where(x => !string.IsNullOrWhiteSpace(x.Name) && x.Name.Contains(name, StringComparison.InvariantCultureIgnoreCase)).ToList();
#endif
                    Apps = apps.GroupBy(x => x.Id).ToDictionary(x => x.First().Id, x => x.First());
                    int index = 0;
                    Parallel.ForEachAsync(Apps, async (app, token) =>
                        {
                            worker.QueueBackgroundWorkItem(async token =>
                            {
                                await CheckImageAsync(app.Value, index);
                                index++;
                            });
                        });

                }

                return Apps;
            }
        }

        public async Task CheckImageAsync(GameInfo app, int index)
        {
            using (var imgClient = new HttpClient())
            {
                try
                {
                    var result = await imgClient.GetAsync(app.Image);
                    app.HasPicture = result.StatusCode == HttpStatusCode.OK;
                }
                catch (Exception ex)
                {
                    app.HasPicture = false;
                    logger.LogWarning($"{app.Id} {app.Name} : {ex.Message}");
                }

                if (!app.HasPicture)
                {
                    logger.LogWarning($"{index}. Unable to find picture for the app {app.Id} : {app.Name}");

                }
            }
        }
        public async Task<IEnumerable<GameInfo>?> SearchAsync(string pattern)
        {
            return await Task.Run(() => Apps.Where(x => x.Value.IsAvailable && x.Value.Name.ToLower().Contains(pattern, StringComparison.InvariantCultureIgnoreCase)).Select(x => x.Value));
        }

        public async Task<GameInfo?> GetAppInfoAsync(long appId)
        {
            using (var client = new HttpClient())
            {
                var result = await client.GetAsync($"https://store.steampowered.com/api/appdetails?appids={appId}");
                if (result.IsSuccessStatusCode)
                {
                    var model = await result.Content.ReadAsStringAsync();
                    var obj = (JObject)JObject.Parse(model)[appId.ToString()];
                    if (obj != null && obj.Value<bool>("success"))
                    {
                        var game = obj["data"].ToObject<GameInfo>();
                        game.Id = appId;
                        return game;
                    }
                }
                return null;
            }
        }


        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            var task = GetAppsAsync();
            if (!task.IsCompleted)
            {
                return task;
            }

            return Task.CompletedTask;
        }

        public IList<GameInfo>? GetAppsByIds(params long[] id)
        {
            if (id == null) return new List<GameInfo>();
            return Apps.Where(x => x.Value.IsAvailable && id.Contains(x.Key)).Select(x => x.Value).ToList();
        }

        public GameInfo? GetAppById(long id)
        {
            if (Apps.ContainsKey(id))
                return Apps[id];
            return null;
        }
    }
}
