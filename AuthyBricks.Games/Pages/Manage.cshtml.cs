using AuthyBricks.Games.Api;
using AuthyBricks.Shared;
using AuthyBricks.Shared.Steam;

using IdentityModel.Client;

using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

using Newtonsoft.Json;

using System.IO;

namespace AuthyBricks.Games.Pages
{
    [Authorize]
    public class ManageModel : PageModel
    {
        public IDictionary<long, GameInfo> Games { get; set; }
        ISteamDbService steam;
        IUserManager userManager;

        public ManageModel(ISteamDbService steam, IUserManager userManager)
        {
            this.steam = steam;
            this.userManager = userManager;

        }

        public async Task OnGetAsync()
        {
            if (await userManager.IsSignedIn(User) && userManager.CurrentUser.IsInRole("manager"))
            {
                Games = steam.Apps;
            }
            else
            {
                Response.Redirect("/");
            }
        }

        public async Task<IActionResult> OnPostToggleGameAsync([FromBody] long? id)
        {
            if (await userManager.IsSignedIn(User) && id.HasValue && userManager.CurrentUser.IsInRole("manager"))
            {
                var game = steam.GetAppById(id.GetValueOrDefault());
                game.IsAvailable = !game.IsAvailable;

                return new JsonResult(game);
            }

            HttpContext.Response.StatusCode = 403;
            return null;
        }
    }
}
