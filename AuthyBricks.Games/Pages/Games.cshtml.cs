using AuthyBricks.Games.Api;
using AuthyBricks.Shared;
using AuthyBricks.Shared.Steam;

using IdentityModel.Client;

using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

using Newtonsoft.Json;

using System.IO;

namespace AuthyBricks.Games.Pages
{
    public class GamesModel : PageModel
    {
        [BindProperty]
        public int GamesTotal { get; set; }
        public long[] UserGameIds { get; set; }
        ISteamDbService steam;
        IUserManager userManager;

        public GamesModel(ISteamDbService steam, IUserManager userManager)
        {
            this.steam = steam;
            this.userManager = userManager;

        }

        public async Task OnGet()
        {
            UserGameIds = new long[0];
            GamesTotal = steam.Apps.Where(x => x.Value.IsAvailable).Count();
            if (await userManager.IsSignedIn(User))
            {
                UserGameIds = userManager.CurrentUser.Games.GameIds;
            }
        }
        public async Task<IActionResult> OnGetSearchGames(string pattern)
        {
            var games = await steam.SearchAsync(pattern);
            return new JsonResult(games);
        }

        public async Task<IActionResult> OnPostPlayAsync([FromBody] long? id)
        {
            if (await userManager.IsSignedIn(User))
            {
                var game = await steam.GetAppInfoAsync(id.GetValueOrDefault());
                await userManager.ToggleGameForUser(userManager.CurrentUser, game);
                await userManager.UpdateAsync(userManager.CurrentUser);

                return new JsonResult(game);
            }
            HttpContext.Response.StatusCode = 403;
            return null;
        }
    }
}
