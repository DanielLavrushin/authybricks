using AuthyBricks.Games.Api;
using AuthyBricks.Shared;
using AuthyBricks.Shared.Steam;

using IdentityModel.Client;

using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

using Newtonsoft.Json;

using System.IO;

namespace AuthyBricks.Games.Pages.Account
{
    [Authorize]
    public class ProfileModel : PageModel
    {
        IUserManager userManager;
        IIdentityService identityService;
        ISteamDbService steam;
        public ICollection<GameInfo> Games { get; set; }

        [BindProperty]
        public string Theme { get; set; }
        [BindProperty]
        public string FullName { get; set; }

        [BindProperty]
        public bool IsSubscribed { get; set; }

        public ProfileModel(IUserManager userManager, IIdentityService identityService, ISteamDbService steam)
        {
            this.identityService = identityService;
            this.userManager = userManager;
            this.steam = steam;
        }

        public async Task<IActionResult> OnGet()
        {
            await userManager.GetUserAsync();
            if (userManager.CurrentUser == null)
            {
                await HttpContext.SignOutAsync();
                return new RedirectToPageResult("/Index");
            }
            FullName = userManager.CurrentUser.FullName;
            Theme = userManager.CurrentUser.Theme;
            IsSubscribed = userManager.CurrentUser.IsSubscribed.GetValueOrDefault();
            Games = steam.GetAppsByIds(userManager.CurrentUser.Games.GameIds);
            return Page();
        }


        public async Task<IActionResult> OnPostAsync()
        {
            await userManager.GetUserAsync();
            userManager.CurrentUser.FullName = FullName;
            userManager.CurrentUser.IsSubscribed = IsSubscribed;
            userManager.CurrentUser.Theme = Theme;
            await userManager.UpdateAsync(userManager.CurrentUser);
            return RedirectToPage();
        }

        public async Task<IActionResult> OnPostSignOutAsync()
        {
            await HttpContext.SignOutAsync();
            return RedirectToPage("/");
        }
    }
}
