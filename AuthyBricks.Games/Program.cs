using AuthyBricks.Games.Api;

using IdentityModel;

using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.OAuth.Claims;
using Microsoft.IdentityModel.Tokens;

var builder = WebApplication.CreateBuilder(args);

var services = builder.Services;
builder.Services.ConfigureApplicationCookie(options =>
{
    options.LoginPath = $"/identity/account/login";
    options.LogoutPath = $"/identity/account/logout";
});
services.AddAuthentication(options =>
{
    options.DefaultScheme = "cookie";
    options.DefaultChallengeScheme = "oidc";
})
            .AddCookie("cookie")
            .AddOpenIdConnect("oidc", options =>
            {
                options.Authority = "https://localhost:7000";
                options.ClientId = "authyBricks.Games";
                options.ClientSecret = "password123";

                options.ResponseType = "code";
                options.UsePkce = true;
                options.ResponseMode = "query";

                options.GetClaimsFromUserInfoEndpoint = true;
                options.Scope.Add(JwtClaimTypes.Email);
                options.Scope.Add(JwtClaimTypes.Role);
                options.Scope.Add("authyBricks.Games.users");

                options.SaveTokens = true;
            });

services.AddHttpContextAccessor();
services.AddHostedService<CheckSteamDbImageService>();
services.AddSingleton<SteamDbTaskWorker>();
services.AddSingleton<IIdentityService, IdentityService>();
services.AddSingleton<ISteamDbService, SteamDbService>();
services.AddHostedService<ISteamDbService>(provider => provider.GetService<ISteamDbService>());
services.AddScoped<IUserManager, UserManager>();
services.AddControllersWithViews().AddRazorRuntimeCompilation();
services.AddRazorPages();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthentication();
app.UseAuthorization();

app.MapRazorPages();

app.Map("/signout", async (HttpContext context) =>
{
    await context.SignOutAsync();
    context.Response.Redirect("/");
});

app.Run();
